# Combustion Kinetics
# Course created for undergraduate module on Combustion Kinetics in Chemical Engineering at IISER Bhopal

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/sssparsh14%2FChemical-Kinetics-for-Combustion/master)


![Website](https://img.shields.io/website/https/github.com)


   + [**IISER Bhopal**](https://iiserb.ac.in/), Fall 2020
   + [**Dept. of Chemical Engineering**](https://che.iiserb.ac.in/index.php) (Chemical Engineering)
   + [**Dr.-Ing. Sparsh Sharma**](https://sparsh-sharma.github.io/) (sparsh.sharma@b-tu.de)

![](ck1.jpg)

### Description
Chemical kinetics is the study of the rates of chemical reactions; if changes in conditions impact the speed of a reaction, we can better understand what caused the reaction. In this course, you’ll learn to understand complicated data sets and analysis techniques for measuring and understanding these rate changes. You’ll study the origin of kinetics rate laws, emerging tools, techniques and topics of interest to chemical engineers, material scientists, chemists and biologists.

The goal of this course is to present to students of chemical engineering an interconnected set of computational methods needed in the core undergraduate chemical engineering curriculum. In particular, methods that assist the students in solving problems in the core areas of chemical reaction equilibria, separations, unit operations, and chemical reactor engineering.

### Learning Outcomes
On completion of this course, the student will be able to:
1) Perform material balances to derive general reactor design equations and use the appropriate reaction kinetics in the reactor design equations.
2) Express concentrations and molar flowrates in terms of conversion. Perform energy balances for the basic reactor types and use the energy balances for reactor design.
3) Extend these operations to the case of multiple reactions and reactor sequences. Overall, combine material balance, rate law, stoichiometry, energy balance for optimal reactor design and operation.
4) Develop fundamentals of catalysis from catalytic reactions to catalyst deactivation as well as transport processes from binary diffusion to mass transfer and reaction in a packed bed.
5) Present catalytic reactor design concepts including thermodynamic operational window as well as requisite radial and axial mixing and catalyst particle size evaluation.


Thanks in advance for inputs to improve this course.\
Regards,\
Sparsh Sharma, PhD
